// console.log("Hello World");

// 1-4
let num1 = parseInt(prompt("Enter a number."));
let num2 = parseInt(prompt("Enter another number."));
sum = num1 + num2;
if (sum < 10) console.warn("The total is "+sum+".\n The output is "+sum+".");
else if(sum >= 10 && sum < 20) alert("The total is "+sum+".\n The output is "+(num1-num2)+".");
else if(sum >= 20 && sum < 30) alert("The total is "+sum+".\n The output is "+(num1*num2)+".");
else if(sum >= 30) output = alert("The total is "+sum+".\n The output is "+(num1/num2)+".");
else alert("The input is not a number.");


// 5
let nameInput = prompt("Enter Name: ");
let ageInput = parseInt(prompt("Enter Age: "));
if (!nameInput || !ageInput) console.log("Are you a time traveller?");
else console.log("Name: "+nameInput+"\nAge: "+ageInput);

// 6
function isLegalAge(age) {
	if (age < 18) alert("You are not allowed here.");
	else alert("You are of legal age.");
}
isLegalAge(ageInput);

// 7
switch(true)
{
	case (ageInput < 18 && ageInput > 0):
		alert("You are not allowed to party.");
		break;
	case (ageInput == 18):
		alert("You are now allowed to party.");
		break;
	case (ageInput <= 21):
		alert("You are now part of the adult society.");
		break;
	case (ageInput <= 65):
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?");
		break;
}

// 8
try {
	throw "Error!";
}
catch(error) {
	alert(error);
}
finally {
	isLegalAge(ageInput);
}